---
title: Meetups
layout: default
---

# List of Meetups


<div class="home">

  <!-- <h1 class="page-heading">List of Meetups</h1> -->
  
	<ul class="post-list">
	{% if page.url == "/meetups/" %}
			{% for post in site.tags.pinned %}
						<article class="post">
							<p class="pinned-article"><b>📌 Pinned Meetup</b></p>

							<h1>
									<a href="{{ site.baseurl }}{{ post.url }}">
											{{ post.title }}
									</a>
							</h1>

							<div class="entry">
									<p>
											{{ post.excerpt }}
									</p>
							</div>

							<a href="{{ site.baseurl }}{{ post.url }}" class="read-more">
									Read More ⟶
							</a>
						</article>
			{% endfor %}
	{% endif %}
	</ul>


  <ul class="post-list">
    {% for post in site.posts %}
		{% unless post.tags contains 'pinned' %}
      <li>
        <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>

        <h2>
          <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
        </h2>
      </li>
		{% endunless %}
    {% endfor %}
  </ul>

  <p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a></p>

</div>




