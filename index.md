---
layout: default
---


<div class="home">
<!--
  <h1 class="page-heading">Posts</h1>

  <ul class="post-list">
    {% for post in site.posts %}
      <li>
        <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>

        <h2>
          <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
        </h2>
      </li>
    {% endfor %}
  </ul>

  <p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a></p>
-->
</div>


# Welcome to SciComm!

If you would like to join our communication channels you can join the Telegram group and/or the Google Groups -

- Telegram: [@ashokascicomm](https://t.me/ashokascicomm)
- Google Groups: [g/scicomm-ashoka](https://groups.google.com/g/scicomm-ashoka)

Most of the communication will be happening on these channels. You can find a list of our past and upcoming meetups [here](/meetups/). Please make sure to go through our [Code of Conduct](/CodeOfConduct/) and follow it in online and offline spaces.

# Frequently Asked Questions (FAQs)

- **How do I propose a talk?** \\
You can head to our GitLab [talks repository](https://gitlab.com/SciComm/talks) and create an issue there. You'll need a GitLab account (free to create) to submit an issue. The steps to create an issue are - \\ 
1. Go to [https://gitlab.com/SciComm/talks](https://gitlab.com/SciComm/talks) and login.
2. On the side bar click "Issues"
3. Click the "New Issue" button.
4. Fill out the details of your talk 

- **How do I stay updated about upcoming meetups?** \\
Join our [Telegram](https://t.me/ashokascicomm) and/or [google groups](https://groups.google.com/g/scicomm-ashoka) (effectively a mailing list). We discuss things on the Telegram group but the Google groups will always get the updates such as upcoming meetups and any other announcement.

- **Ask more questions!** \\
If something isn't covered here, please do ask on any of our channels of communication!
